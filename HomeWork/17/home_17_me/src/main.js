const Koa = require('koa')
const koaBody = require('koa-body')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()

render(app, {
	root: path.join(process.cwd(), 'views'),
	layout: 'template',
	viewExt: 'ejs',
	cache: false
  })

const stripPrefix = async (ctx, next) => {
	if (!ctx.path.startsWith('/-')) {
		ctx.status = 404
		return
	}

	ctx.path = ctx.path.slice(2)
	await next()
}

app.use(koaBody({ multipart: true }))
app.use(require('./route'))

app.use(stripPrefix)
app.use(serve(path.join(process.cwd(),'public')))

app.listen(8000)
