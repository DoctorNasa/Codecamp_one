const Koa = require('koa')
const app = new Koa()
const Router = require('koa-router')
const router = new Router()
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')
const util = require('util')

let readSaraly = util.promisify(fs.readFile)

router.get('/', async ctx => {
  // let  sara= await readSaraly('public/homework2_1.json', 'utf8')
  // console.log(sara)
  // // await ctx.render('landing')
})

router.get('/about', async ctx => {
  await ctx.render('about')
})
router.get('/tablepeople', async ctx => {
  let sara = await readSaraly('public/homework2_1.json', 'utf8')
  await ctx.render('tablepeople', {people: JSON.parse(sara)})
})

router.get('/portfolio', async ctx => {
  await ctx.render('portfolio')
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})
app.listen(3300)
console.log(__dirname)
