
นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร และจำนวนกี่คอร์ส ให้ใช้แค่ statement เดียวเท่านั้น
select students.name, SUM(courses.price),count(*)
 from students right JOIN enrolls on enrolls.student_id = 
 students.id INNER JOIN courses on courses.id = enrolls.course_id GROUP BY students.id 

ลองเพิ่มนักเรียน ลงเรียนเพิ่มอีกหนึ่งวิชา
insert into enrolls (student_id,course_id) values ( 102,10);

นักเรียนแต่ละคน ซื้อคอร์สไหน ราคาแพงสุด
select students.name, max(courses.price),count(*)
 from students right JOIN enrolls on enrolls.student_id = 
 students.id INNER JOIN courses on courses.id = enrolls.course_id GROUP BY students.id 
นักเรียนแต่ละคนซื้อคอร์สราคาเฉลี่ยคนละเท่าไหร่
select students.name, Avg(courses.price),count(*)
 from students right JOIN enrolls on enrolls.student_id = 
 students.id INNER JOIN courses on courses.id = enrolls.course_id GROUP BY students.id 
Homework14-2

1. select id, name, SUM(price) as enrolled_price, count(*) as enrolled from (select students.id, students.name, courses.price from enrolls right join students on students.id = enrolls.student_id left join courses on courses.id = enrolls.course_id) as t GROUP BY id;

2. select id, student_name, max(price) from (select students.id, students.name as student_name, courses.id as course_id, courses.name as course_name, courses.price from enrolls right join students on students.id = enrolls.student_id left join courses on courses.id = enrolls.course_id) as t GROUP BY id;

3. select id, student_name, avg(price) from (select students.id, students.name as student_name, courses.id as course_id, courses.name as course_name, courses.price from enrolls right join students on students.id = enrolls.student_id left join courses on courses.id = enrolls.course_id) as t GROUP BY id;





Homework14-3

1. create VIEW enroll_details as select student_name, course_name, detail, price, (case when instructor is null then 'No Instructor' else instructor END) as instructor from (select students.id, students.name as student_name, courses.name as course_name, courses.detail, courses.price, courses.teach_by, instructors.name as instructor from enrolls right join students on students.id = enrolls.student_id left join courses on courses.id = enrolls.course_id left join instructors on instructors.id = courses.teach_by) as t UNION SELECT 'Total','','', SUM(price),'' from (select students.id, students.name as student_name, courses.name as course_name, courses.detail, courses.price, courses.teach_by, instructors.name as instructor from enrolls right join students on students.id = enrolls.student_id left join courses on courses.id = enrolls.course_id left join instructors on instructors.id = courses.teach_by) as s;