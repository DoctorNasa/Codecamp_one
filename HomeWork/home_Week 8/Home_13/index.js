const Koa = require ('koa')
const Router = require('koa-router')
const serve = require ('koa-static')
const path = require('path')
const render = require('koa-ejs')
const util = require ('util')
const mysql = require('mysql2/promise')
const app = new Koa()
const router = new Router()
const pool  =  mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'onlinecoure'
})



router.get('/online', async ctx => {
  let data={}
  const [rows,fields] = await pool.query('select instructors.name from courses right join instructors on instructors.id = courses.teach_by where courses.name is null;')
   data.instuctor = rows
  console.log(data.instuctor)
  const [rows1,fields1] = await pool.query('select courses.name from courses left join instructors on instructors.id = courses.teach_by where instructors.name is null;')
  data.coures = rows1
  console.log(data.coures)
  await ctx.render('online',data)
  console.log(data)

})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'templace',
    viewExt: 'ejs',
    cache: false
  })
app.listen(3500)
