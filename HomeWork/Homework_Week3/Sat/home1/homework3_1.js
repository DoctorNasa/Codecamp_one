fetch("homework2_1.json")
  .then(response => {
    return response.json();
  })
  .then(Json => {
    let employees = Json["peopleSalary"];
    addYearsalary(employees[0]);
    addnextSalary(employees[0]);
    addAdditionalFields(employees);
    //console.log(Json);
  })
  .catch(error => {
    console.error("Error:", error);
  });

function addYearsalary(employees) {
  employees["yearSalary"] = employees["salary"] * 12;
  
}
let numberofYearsSalary = 3; //กำหนดloopเพื่อเปลี่ยนค่่าเงินเดือนกี่ปีง่ายๆ
function addnextSalary(employees) {
  employees["nextSalary"] = [];
  employees["nextSalary"][0] = employees["salary"]; //400000
  // employees['nextSalary'][1] = employees['salary']*.1+employees['salary'];
  // employees['nextSalary'][2] = employees['salary']*.1+employees['nextSalary'][1];
  for (
    i = 1;
    i < numberofYearsSalary;
    i++ //โดยที่กำหนดค่า i ให้เริ่มต้นที่ปีที่สอง และ i<numberofYearsalaryให้เป็นloop ข้างบน
  ) {
    let nowlySalary = employees["nextSalary"][i - 1];
    employees["nextSalary"][i] = nowlySalary + nowlySalary * 0.1;
  }
  
}
function addAdditionalFields(employees) {
    
  for (let i = 0; i < employees.length; i++) {
    addYearsalary(employees[i]);
    addnextSalary(employees[i]);
  }
  

  let mytable = "<tr>"; //หัวข้อ
  for (let i in employees[0]) {
    mytable += "<th>" + i + "</th>"; //+= เพิ่มไปเรื่อยๆ บวกไปเรื่อยๆ เอามาต่อกัน
  }
  mytable += "</tr>";

  for (let i = 0; i < employees.length; i++) {
    mytable += "<tr>"
    for (let y in employees[i]) {
    
    if (y!== "nextSalary"){
        mytable += "<td>" + employees[i][y]+ "</td>";
    }
    else{
        mytable +='<td><ol>'
        for (let r in employees[i]['nextSalary'])//rไปรับค่าจะnextSalary ค่าเริ่มต้น
        mytable+= '<li>'+ employees[i]['nextSalary'][r] +'</li>' 
      }
    }

  mytable += '</ol></td></tr>';

  }
  console.log(mytable);
  $("#employeetable").append($(mytable));
}
