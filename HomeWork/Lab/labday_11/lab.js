const Koa = require('koa')
const Router = require('koa-router')
const app = new Koa()
const router = new Router()
app.use(async (ctx, next) => {
   console.log(ctx.path + ' : 1');
   await next();
})
app.use(async (ctx, next) => {
   console.log('2');
   await next();
})
app.use(async (ctx, next) => {
   console.log('3');
   await next();
})


router.get('/', async ctx => {
    ctx.myVariable = 'forbidden'; // assume variable
    await next();
 })
 async function myMiddleware (ctx, next) {
    if (ctx.myVariable == 'forbidden')
        ctx.body = "Access Denied";
    else
        await next();
 }
 app.use(myMiddleware)
 app.use(router.routes())
 app.use(myMiddleware)
 app.use(myMiddleware)
 app.listen(3000)
 