const Koa = require('koa')
const app = new Koa()
const koaBody = require('koa-body')


app.use(koaBody())

app.use(ctx => {
    // Content-Type: text/plain; charset=utf-8
    // Cache-Control: private, max-age=0
    ctx.set('Content-Type', 'text/plain; charset=utf-8')
    ctx.set('Cache-Control', 'private, max-age=0')
    ctx.status = 401 // Unauthorized
    ctx.body = 'you must log in first'
    console.log(ctx.request.body)
   })


app.listen(3000)