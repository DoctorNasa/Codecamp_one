const Koa = require('koa')
const Router = require('koa-router')
const app = new Koa()
const router = new Router()
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

router.get('/',async ctx => {
  await ctx.render('landing') 
})

router.get('/contact', ctx => {
  ctx.body = 'this is contact page'
})

router.get('/about', ctx => {
  ctx.body = 'this is about page'
})
router.get('/image', ctx => {
  ctx.body = `<img src="./images/oil.jpg">`
})
render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
 }) 

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

// app.use(async ctx => {
//     console.log(ctx.path)
//     if (ctx.path === '/about'){
//         ctx.body='this is me'
//     }else {
//         ctx.body = 'Hello World'
//     }
// })
app.listen(2000)