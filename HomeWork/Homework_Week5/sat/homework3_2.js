"use strict";
fetch("homework2_1.json")
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    const peopleSalary = myJson;
    let peopleLowSalary = peopleSalary
      .filter(val => {
        return val.salary < 100000;
      })
      .map(val => {
        val.salary = val.salary * 2;
        return val;
      });
    let sumSalary =
      parseInt(
        peopleSalary
          .filter(val => {
            return val.salary >= 100000;
          })
          .map(val => {
            return parseInt(val.salary);
          })
          .reduce((sum, val) => {
            return sum + val;
          }, 0)
      ) +
      parseInt(
        peopleLowSalary
          .map(val => {
            return parseInt(val.salary);
          })
          .reduce((sum, val) => {
            return sum + val;
          }),
        0
      );

    // for (const key in peopleSalary) {
    //   if (peopleSalary[key].salary >= 100000) {
    //     $("#table_body").append(
    //       `<tr><td>${peopleSalary[key].firstname}</td><td>${
    //         peopleSalary[key].lastname
    //       }</td><td>${peopleSalary[key].company}</td><td>${
    //         peopleSalary[key].salary
    //       }</td></tr>`
    //     );
    //   }
    // }

    for (const key in peopleLowSalary) {
      $("#table_body").append(
        `<tr>
        <td>${peopleLowSalary[key].id}</td>
        <td>${peopleLowSalary[key].firstname}</td>
        <td>${peopleLowSalary[key].lastname}</td>
        <td>${peopleLowSalary[key].company}</td>
        <td>${peopleLowSalary[key].salary}</td></tr>`
      );
    }
    $("#table_body").append(
      `<tr><td colspan="4">รวม</td><td>${sumSalary}</td></tr>`
    );
    console.log(sumSalary);
    //console.log(myJson);
  });
