"use strict";
fetch("homework1-4.json").then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    const people = myJson
    let man = people.filter((val)=>{
      return val.gender === 'male' && val.friends.length >= 2
    }).map((val)=>{
      let balance = val.balance.replace('$', '').replace(',', '')
      let int_balance = parseInt(balance)/10
      const { name, gender, company, email, friends,  } = val
      return { name, gender, company, email, friends, balance: `$${int_balance}`}
    })

    let table = man.reduce((sum, val)=>{
      let friends = val.friends.reduce((sum, val, index)=>{
          return sum+`<li>${val.name}</li>`
      }, '')
      return sum+`<tr><td>${val.name}</td><td>${val.gender}</td><td>${val.company}</td><td>${val.email}</td><td><ol>${friends}</ol></td><td>${val.balance}</td></tr>`
    }, '')

    $('#table_body').html(table);
    console.log(man);
  });