//Creates a new array by manipulating the values in another array. Great for data
// manipulation and it is often used in React becuse it is an immutale method.

//Ex. created an array that adds a $ to the beginning of each number.

// const numbers = [2,3,4,5,6,7,9]
// const dollars = numbers.map ( number => '฿' + number)
// console.log(dollars)

// for (i =0; i<2; i++){
//   let i = dollars
// console.log(i)
// }

/* --------------------------------------------------------- */
/*
สำหรับ Method Map นั้นจะคล้ายกับ ForEach เลย คือค่าใน Array แต่ละตัวจะทำการนำไปผ่าน Function
 ที่เรากำหนดไว้ แต่ Map นั้น สามารถ return ค่าออกมาได้โดยค่าที่ return ออกมาจะเป็น Array ซึ่งสามารถนำไปทำใน 
 Method อื่นได้ (จะกล่าวเพิ่มเติมในช่วงท้ายของบทความ) ตัวอย่างของการทำงานของ Map เป็นดังนี้ */

//  const numbers = [35, 23, 17, 21]
// const result = numbers.map((number) => {
//   return number*2
// })
// console.log(result) // [70, 46, 24, 42]

// //สามารถเขียนโดยใช้ For Loop ที่ให้ผลลัพธ์ที่เหมือนกันได้ดังนี้

const numbersloop = [35, 23, 17, 21]
let result = []
for (let i = 0; i < numbersloop.length; i++) {
  result.push(numbersloop[i] * 2)
}
console.log(result) // [70, 46, 34, 42]
/* --------------------------------------------------------- */