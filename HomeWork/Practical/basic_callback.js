function func () {
  function func2 (callback) {
    return callback()
  }
  function tempFunc() {
    return 12
  } 
  let y = func2(tempFunc)
  return y
}
func()
//ต้องการได้ค่าจากTempFucnให้return 12ออกมา //12
console.log(func())

function func () {
  function func2 (callback) {
    callback()
    return callback()
  }
  func2(function () {
    return 12
})
  return func2
}
  func()
  /*ƒ func2(callback) {
      callback();
      return callback();
    } ตัวอย่างผิดไม่สามารถretrunค่าออกมาได้จะได้ค่าfunc2*/

function func2 () {
  var c = 12;
  console.log(window.c)
}
/*การประกาศตัวแปลแบบ varค่าสามารถประกาศค่าซ่ำได้ต่อเนื่อง ทำให้เมื่อมีการประกาศ
 จะไม่สามารถระบุบได้เมื่อต้องการหาค่านั้น และจะแก้ไขปัญหา ด้วยการประกาศ
 let ค่ือใช้เฉพาะเจาะจง 
 Windown ค่าการประกาศรูปแบบ univers แบบเดี่ยวกับVar*/

//---loop-------//
 for (var i=0; i< 10; i++) {
    console.log(i)
  } //loop

  for (var i=0; i< 10; i++) {
    console.log(i)
  }//loop

  for (let j=0; j< 10; j++) {
    console.log(j)
  }//loop


  for (var i=0; i< 10; i++) {
    console.log(i)
  } //ออกเป็น 0 1 2 3 4 5 6 7 8 9

  for (; i< 10; i++) {
    console.log(i)
  }//ทำอีกรอบจะเปลี่ยนเป็น undefined เพราะทำไปแล้ว
  for (; i< 20; i++) {
    console.log(i)
  }/* 10
 11
 12
 13
 14
 15
 16
 17
 18
 19 
 */
for (let j=0; j< 10; j++) {
    console.log(j)
  }//ออกเป็น 0 1 2 3 4 5 6 7 8 9

  for (j=1; j< 10; j++) {
    console.log(j)
  }//ออกเป็น 1 2 3 4 5 6 7 8 9

  function func() {
    function func2() {
      e = 12;
    }
    func2();
  }
  เรียก
  func()
  //12
  console.log(func()) //12

  function func() {
    e = 9;
    function func2() {
      e = 12;
    }
    func2()
  }
  //เรียน func2() //undefined
  //แต่ e //12
  