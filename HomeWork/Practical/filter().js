const studentsAge = [10,11,12,14,18,20,21,23,24,25,19,16]
const aleToDrink = studentsAge.filter (age => age > 18);
console.log(aleToDrink);
//aleToDrink will be equal to Array(6) [20, 21, 23, 24, 25, 19]

const studentsAge = [10,11,12,14,18,20,21,23,24,25,19,16]
let result = []
for (let i = 0; i < studentsAge.length; i++) {
  if (studentsAge[i].age > 18) {
     result.push(members[i])
  }
}
console.log(result)

//สำหรับ Method Filter จะมีหน้าที่ไว้คัดกรองถ้าสมาชิกใน Array ตัวไหนที่เงื่อนไขได้ตามที่ทำหนดไว้ใน Function จะถูกนำมาใส่รวมกันใน Array ใหม่ที่เป็นผลลัพธ์ ตัวอย่างของการทำงาน Method Filter ดังนี้

/* --------------------------------------------------------- */
const members = [ 
   {name: "Eve", age: 24}, 
   {name: "Adam", age: 48}, 
   {name: "Chris", age: 18}, 
   {name: "Danny", age: 30}
]
const result = members.filter((member) => {
  return member.age > 25
})
console.log(result)

// [{name: "Adam", age: 48}, {name: "Danny", age: 30}]

//สามารถเขียนโค้ดให้ได้ผลลัพธ์ตามตามบนโดยใช้ For loop ได้ดังนี้

const members = [ 
   {name: "Eve", age: 24}, 
   {name: "Adam", age: 48}, 
   {name: "Chris", age: 18}, 
   {name: "Danny", age: 30}
]
let result = []
for (let i = 0; i < members.length; i++) {
  if (members[i].age > 25) {
     result.push(members[i])
  }
}
console.log(result)
// [{name: "Adam", age: 48}, {name: "Danny", age: 30}]
/* --------------------------------------------------------- */