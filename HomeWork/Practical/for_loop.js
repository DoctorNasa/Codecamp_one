//ต้องการได้ค่า Array ใหม่โดยแต่ละตัวนั้นค่าเป็น 2 เท่าจากเดิม
const numbers = [2,3,4,5,6]
let result = []
for (let i = 0; i < numbers.length; i++) {
   result.push(numbers[i]*2)
}
console.log(result) // [4,6,8,10,12]
